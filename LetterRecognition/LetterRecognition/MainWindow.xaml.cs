﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LetterRecognition
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonAqcuireSchemaFromFolder_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.ShowDialog();
            MessageBox.Show(folderBrowser.SelectedPath);
        }

        private void buttonAqcuireSchemaFromFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog fileBrowser = new System.Windows.Forms.OpenFileDialog();
            if (fileBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = fileBrowser.FileName;
                ImageSource source = new BitmapImage(new Uri(path));
                letterPicture.Source = source;
            }
        }
    }
}
